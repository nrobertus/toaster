#include "Arduino.h"
#include "SoftwareSerial.h"
#include "DFRobotDFPlayerMini.h"

SoftwareSerial mySoftwareSerial(11, 10); // RX, TX
DFRobotDFPlayerMini myDFPlayer;

#define inputPin 12
#define READ_COUNTS 100
#define SONG_COUNT 5

int timesRead = 0;
int toasting = 0;

unsigned long startTime = 0UL;
unsigned long delayTime = 126000000UL; // 35 hours. This is so evil.
unsigned long maxTime = 2160000001UL;  // 25 days for reset time

unsigned long seconds = 1000L; // !!! SEE THE CAPITAL "L" USED!!!
unsigned long minutes = seconds * 60;

void (*resetFunc)(void) = 0;

void _play_song(int index)
{
  delay(2000);
  myDFPlayer.play(index);
}

void playYaketySax()
{
  _play_song(1);
}

void playOldTownRoad()
{
  _play_song(2);
}

void playBreakfast()
{
  _play_song(3);
}

void playAllStar()
{
  _play_song(4);
}

void playShiaLeBeouf()
{
  _play_song(5);
}

void playDing()
{
  myDFPlayer.play(7);
}

void playScream()
{
  myDFPlayer.play(6);
}

void playRandomSong()
{
  int songIndex = random(1, SONG_COUNT + 1);
  _play_song(songIndex);
}

void setup()
{
  // Set up the I/O
  pinMode(inputPin, INPUT_PULLUP);
  pinMode(LED_BUILTIN, OUTPUT);

  // Set up serial connections
  mySoftwareSerial.begin(9600);

  // Instantiate DF player and set the volume
  if (!myDFPlayer.begin(mySoftwareSerial))
  {                        //Use softwareSerial to communicate with mp3.
    myDFPlayer.volume(30); //Set volume value. From 0 to 30
  }
}

void loop()
{
  int input = digitalRead(inputPin);

  if (millis() - startTime >= delayTime) // welcome to unsigned math
  {

    startTime += delayTime; // startBlink gets moved up to current step

    if (startTime > maxTime)
    {
      resetFunc();
    }
    else
    {
      playScream();
    }
  }

  if (input == 0)
  {
    if (toasting == 0)
    {
      // We used to be not toasting. Now we are.
      int randomNumber = random(0, 2);
      if (randomNumber > 0)
      {
        int randomDelayS = random(15, 45);
        unsigned long randomDelayMS = randomDelayS * 1000UL;
        delay(randomDelayMS); // Wait a minutes TODO change to minutes
        playDing();   // And hit 'em with the old razzle dazzle
      }
    }
    toasting = 1;
    digitalWrite(LED_BUILTIN, HIGH);
    timesRead = 0;
  }
  else
  {
    if (timesRead >= READ_COUNTS)
    {
      if (toasting == 1)
      { // We used to be toasting. Now we aren't.
        playRandomSong();
      }
      toasting = 0;
      digitalWrite(LED_BUILTIN, LOW);
    }
    else
    {
      timesRead++;
    }
  }

  delay(1);
}